<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Fields\Number;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\LensRequest;

class MostProlificWriter extends Lens
{
    public static function columns(){
        return [
            'users.id',
            'users.name',
            'users.role',
            DB::raw('count(articles.id) as total'),
            DB::raw('count(articles.id) * 100 as wage')
        ];
    }

    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->select(self::columns())
            ->join('articles', 'users.id', '=', 'articles.user_id')
            ->where('articles.is_published', true)
            ->orderBy('total', 'desc')
            ->groupBy('articles.user_id')
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id'),
            Text::make('Nome', 'name'),
            Text::make('Ruolo', 'role'),
            Number::make('Articoli scritti', 'total'),
            Number::make('Guadagni totali', 'wage', function ($value) {
                return '€'.number_format($value, 2);
            }),
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'most-prolific-writer';
    }
}
