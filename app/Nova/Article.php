<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Waynestate\Nova\CKEditor;
use Eminiarts\Tabs\TabsOnEdit;
use Laravel\Nova\Fields\Boolean;
use App\Nova\Filters\IsPublished;
use App\Nova\Filters\PublishingDateFilter;
use App\Nova\Lenses\LastFiveArticles;
use App\Nova\Metrics\NewArticles;
use Laravel\Nova\Fields\KeyValue;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Metrics\ArticlePerUser;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Metrics\ArticlePublishedPerDay;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;

class Article extends Resource
{
    use TabsOnEdit;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Article::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        return [
            new Tabs('Tabs', [
                'Principali' => [
                    ID::make(__('ID'), 'id')->sortable(),
                    
                    Text::make('Titolo', 'title')
                        ->sortable()
                        ->rules('required', 'max:255'),
                ],
                'Corpo e Pubblicazione' => [
                    CKEditor::make('Corpo', 'body')
                        ->options([
                            'height' => 250,
                            'toolbar' => [
                                ['Source', 'Format', 'FontSize'],
                                ['Bold', 'Italic', 'Strike', 'Subscript', 'Superscript'],
                                ['NumberedList', 'BulletedList', 'Outdent', 'Indent'],
                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                ['Cut','Copy','Paste', 'PasteText', 'PasteFromWord', 'RemoveFormat', 'HorizontalRule'],
                            ],
                        ])
                        ->rules('required', 'max:500')
                        ->alwaysShow(),
        
                    Boolean::make('Pubblicato', 'is_published')
                        ->readonly(function($request) {
                            return ! $request->user()->isChiefEditor();
                        }),
        
                    Date::make(__('Pubblicato il'), 'published_at')
                    ->format('DD/MM/YYYY')
                    ->pickerDisplayFormat('d/m/Y')
                    ->readonly(function($request) {
                        return ! $request->user()->isChiefEditor();
                    }),
        
                    Date::make('Creato il', 'created_at')
                        ->format('DD-MM-YYYY')
                        ->pickerDisplayFormat('d/m/Y')
                        ->onlyOnDetail(),
        
                    Date::make('Modificato il', 'updated_at')
                        ->format('DD-MM-YYYY')
                        ->pickerDisplayFormat('d/m/Y')
                        ->onlyOnDetail(),                  
                ],
                'Meta' => [
                    KeyValue::make('Meta tags','meta')
                        ->keyLabel('Proprietà') 
                        ->valueLabel('Valore')
                        ->actionText('Aggiungi elemento')
                        ->rules('json'), 
                ],
                'Gallery' => [
                    Images::make('Gallery', 'gallery')
                        ->conversionOnIndexView('thumb')
                        ->rules('required', 'max:8'),
                ],
            ]),

            BelongsTo::make(
              'user', 'user', \App\Nova\User::class
            )

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            ArticlePerUser::make(),
            ArticlePublishedPerDay::make(),
            NewArticles::make(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            IsPublished::make(),
            PublishingDateFilter::make(),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
            LastFiveArticles::make(),
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
