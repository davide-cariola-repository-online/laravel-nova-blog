<?php

use App\Models\Article;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $articles = Article::all();
    return view('welcome', compact('articles'));
})->name('homepage');


Route::get('/article/show/{article}', function(Article $article){
    return view('show', compact('article'));
})->name('article.show');

