<x-layout>

    <div class="container-fluid m-5 p-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8 p-5 shadow bg-dark">
                <form action="{{route('register')}}" method="post">
                    @csrf
                    <div class="text-center">
                        <h1  class="display-1 mb-5 text-center text-white">Register</h1>
                    </div>
                    <input class="mb-3 form-control" type="text" name="name" placeholder="Username">
                    <input class="mb-3 form-control" type="email" name="email" placeholder="Email">
                    <input class="mb-3 form-control" type="password" name="password" placeholder="Password">
                    <input class="mb-3 form-control" type="password" name="password_confirmation" placeholder="Conferma password">

                    <button class="btn btn-info" type="submit">Registrati</button>
                </form>
            </div>
        </div>
    </div>




</x-layout>