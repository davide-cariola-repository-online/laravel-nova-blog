<x-layout>

    <div class="container-fluid m-5 p-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8 p-5 shadow bg-dark">
                <form action="{{route('login')}}" method="post">
                    @csrf
                    <div class="text-center">
                        <h1  class="display-1 mb-5 text-center text-white">Login</h1>
                    </div>
                    <input class="mb-3 form-control" type="email" name="email" placeholder="Email">
                    <input class="mb-3 form-control" type="password" name="password" placeholder="Password">

                    <button class="btn btn-info" type="submit">Accedi</button>
                </form>
            </div>
        </div>
    </div>




</x-layout>