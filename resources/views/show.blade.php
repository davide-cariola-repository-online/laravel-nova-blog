<x-layout>

<div class="container my-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-8">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    @foreach ($article->getMedia('gallery') as $image)
                        <div class="carousel-item  @if($loop->first) active @endif">
                            <img src="{{$image->getUrl()}}" class="d-block w-100" alt="...">
                        </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
        <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-body">
                      <h5 class="card-title">{{$article->title}}</h5>
                      <p class="small fst-italic">Scritto da: {{$article->user->name}}</p>
                      <hr>
                      <p class="card-text">{!!$article->body!!}</p>
                      <a href="{{route('homepage')}}" class="btn btn-primary">Torna indietro</a>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>


</x-layout>