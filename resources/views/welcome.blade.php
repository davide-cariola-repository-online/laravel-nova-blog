<x-layout>

    <div class="container-fluid p-5 bg-dark text-light">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1 class="display-1">Nova blog</h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row justify-content-center">

            @foreach($articles as $article)
                <div class="col-12 col-md-4 my-2">
                    <div class="card">
                        <img src="{{$article->getFirstMediaUrl('gallery','thumb')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">{{$article->title}}</h5>
                          <p class="small fst-italic">Scritto da: {{$article->user->name}}</p>
                          <a href="{{route('article.show', compact('article'))}}" class="btn btn-primary">Scopri di piu'</a>
                        </div>
                      </div>
                </div>
            @endforeach

        </div>
    </div>
    
    
</x-layout>